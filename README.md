# vscode-snippets

Snippets for (my) Visual Studio Code v1.0.2

## Special Characters

This is a global snippet file with no scoping. To see available snippets type ```ch.```

> *I don't know why, but this snippets do not appear in .md-files. To get them I have to press Ctrl-Space. They are available in .txt, .html and others I tested.*

### Programming Characters hard to reach on German Keyboard

#### Single Characters

- at-sign "@": `ch.at`
- backslash "\\": `ch.bs`
- tilde: "~": `ch.til`
- vertical bar "|": `ch.vb`

#### Brackets (line or multiline)

- curly brackets "{}": `ch.cb`
- round brackets "()": `ch.rb`
- square brackets "[]": `ch.sb`

### German Umlauts

- "ä": `ch.ae`
- "ö": `ch.oe`
- "ü": `ch.ue`
- "Ä": `ch.Ae`
- "Ö": `ch.Oe`
- "Ü": `ch.Ue`
- "ß": `ch.ss`

## Lilypond

This snippets are scoped to language-id *lilypond*. To see available snippets type `li.`

### Templates

There are three templates - type `li.!` to see (only) them.

- Generic Template with one score, staff and voice: `li.!generic`
- Three voices Template: `li.!three`
- Piano Template: `li.!piano`

### General Statements and Commands

#### Simple

- Version: `li.ver`
- Clef (choice): `li.clef`
- Time Signatur (choice): `li.time`
- Key Signatur: `lt.key`
- Tempo: `li.tempo`
- Transpose: `li.trans`
- Manual Beams: `li.bean`
- Markup: `li.mark`
- Override: `li.over`
- Revert: `li.rev`
- Chordnotation (< >): `li.chn`
- Break: `li.br`
- NoBreak: `li.nbr`
- Cadence Toggle (on off): `li.cad`
- Manual Bar: `li.bar`
- Include Preamble: `li.pre`
- Indent: `li.indent`
- Stem Direction (Down Up): `li.stem`
- Triplet: `li.trip`
- Staffsize: `li.size`
- Partial: `li.part`

#### Blocks

- Multi Line Comment: `li.mlc`
- File Header: `li.head`
- Relative: `li.rel`
- Repeat Volta: `li.volta`
- Alternative Ending: `li.alt`
- Staff With Name: `li.staffn`
- Staff Group: `li.group`
- Base Shortest Duration (Context): `li.bsd`
- Global key and meter: `li.glob`
- Merge Note Heads: `li.merge`
- Rhythm Staff: `li.rhythm`

#### Grace Notes

- Grace Note(s): `li.grace`
- Slashed Grace Note(s): `li.sgrace`
- Acciaccatura: `li.acc`
- Appoggiatura: `li.app`

#### Spanner

- Text Spanner Text Override
- Textspanner Direction (Down Up): `li.span`
- Position Barre Spanner: `li.bspan`
- Spanner Start Stop: `li.sss`
- String Number Spanner (use): `li.sns`
- String Number Spanner Function (definition): `li.snfs`

#### Special

- Color Notes (head, stem, beam): `li.color`

### Guitar

#### Fingering

- Finger and String Number Orientation: `li.for`
- Stroke Fingering(choice): `li.sfing`
- Fingering Left Hand (choice): `li.fing`
- Right Hand Finger Definitions: `li.rhd`

#### Other

- Guitar Clef: `li.gclef`
- String Number (choice): `li.sn`
- Glissando: `li.glis`
- Harmonics Toggle (on off): `li.harm`
- Position Barre Spanner: `li.bspan`
- String Number Spanner (use): `li.sns`
- String Number Spanner Function (definition): `li.snfs`
- 2 String Tab: `li.ctab2`

